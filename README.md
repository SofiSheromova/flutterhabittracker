# Трекер привычек

Учебное приложение, сделанное по мотивам [Android-курса](https://www.youtube.com/playlist?list=PLQ09TvuOLytS_vYHtFHQzZJFcnbYCYF6x) от Doubletapp на Flutter.  

Это простое приложение для отслеживания хороших и плохих привычек. Хранение привычек осущестляется как локально, так и на сервере с помощью [API](https://doublet.app/droid/8/api). 

Можно скачать apk по [ссылке](https://drive.google.com/file/d/1HhaBfaKeFgg3uW7XDzgUiMXSlUa7yOfw/view?usp=sharing).  
*\* Примечание: здесь используется токен доступа с курса \**  

## Screenshots
*\* Примечание: дизайн на очень скорую руку \**

| <img width="200" src="content/home.png">| <img width="200" src="content/editor.png"> |
| ---------------------------------------------- | -------------------------------------------- |

## Библиотеки и инструменты

- [Dart](https://dart.dev/)
- [Flutter 2](https://flutter.dev/)
- [Bloc](https://bloclibrary.dev/#/)
- [Provider](https://github.com/rrousselGit/provider)
- [intl](https://github.com/dart-lang/intl)
- [moor](https://github.com/simolus3/moor) для работы с базой данных
- [dio](https://github.com/flutterchina/dio) для работы с сетью

## Архитектура

Bloc + Provider   
Вкратце Bloc похож на MVVM. Логика и UI искусственно разделяются в разные классы, UI слушает стримы блока и реагирует на изменения state.  

## Работа с сетью

Для работы с сетью используется библиотека [Dio](https://pub.dev/packages/dio). Библиотека предлагает хорошие возможности по конфигурации работы с сетью (можно задать стандартные хэдеры, interceptors для логирования и обработки ошибок, простую работу с http запросами).  

## Разработка

При разработке может пригодится плагин intl, который будет генирировать ресурсы при их изменениях. Если для всех IDE.   
Для сборки проекта потребуется ключ API.  
Ключ можно получить в боте [DoubleDroidStudentBot](https://t.me/DoubleDroidStudentBot). Затем создайте в корне проекта файл `keystore.properties` и добавьте туда ключ в следующем формате:
```
    AUTHORIZATION_TOKEN="<INSERT_YOUR_API_KEY>"
```
