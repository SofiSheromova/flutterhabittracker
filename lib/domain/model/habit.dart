import 'package:uuid/uuid.dart';

import './habit_type.dart';
import './periodicity.dart';
import 'habit_priority.dart';

var uuid = Uuid();

class Habit {
  String id;
  String title;
  String description;
  Periodicity periodicity;
  HabitType type;
  HabitPriority priority;
  int color;
  DateTime date;
  List<DateTime> doneDates;

  Habit({
    String? id,
    required this.title,
    required this.description,
    this.periodicity = Periodicity.everyday,
    this.type = HabitType.good,
    this.priority = HabitPriority.low,
    int? color,
    DateTime? date,
    List<DateTime>? doneDates,
  })  : id = id ?? uuid.v1(),
        color = color ?? 0xff9c27b0,
        date = date ?? DateTime.now(),
        doneDates = doneDates ?? List.empty();

  Habit.blank() : this(title: "", description: "");

  void markDone(DateTime date) {
    doneDates.add(date);
  }

  Habit copyWith({
    String? id,
    String? title,
    String? description,
    Periodicity? periodicity,
    HabitType? type,
    HabitPriority? priority,
    int? color,
    DateTime? date,
    List<DateTime>? doneDates,
  }) {
    return Habit(
      id: id ?? this.id,
      title: title ?? this.title,
      description: description ?? this.description,
      periodicity: periodicity ?? this.periodicity,
      type: type ?? this.type,
      priority: priority ?? this.priority,
      color: color ?? this.color,
      date: date ?? this.date,
      doneDates: doneDates ?? [...this.doneDates],
    );
  }
}
