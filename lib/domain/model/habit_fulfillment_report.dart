import 'package:flutterhabittracker/domain/model/habit_type.dart';

class HabitFulfillmentReport {
  HabitType habitType;
  int difference;

  HabitFulfillmentReport({required this.habitType, required this.difference});
}
