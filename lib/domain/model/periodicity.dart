class Periodicity {
  final int repetitionsNumber;
  final int daysNumber;

  const Periodicity(
      {required this.repetitionsNumber, required this.daysNumber});

  Periodicity copyWith({int? repetitionsNumber, int? daysNumber}) {
    return Periodicity(
        repetitionsNumber: repetitionsNumber ?? this.repetitionsNumber,
        daysNumber: daysNumber ?? this.daysNumber);
  }

  static const everyday = Periodicity(repetitionsNumber: 1, daysNumber: 1);
}
