enum HabitPriority { low, middle, high }

extension HabitPriorityExtension on HabitPriority {
  String get value {
    switch (this) {
      case HabitPriority.low:
        return '1';
      case HabitPriority.middle:
        return '2';
      case HabitPriority.high:
        return '3';
    }
  }
}
