import 'package:flutterhabittracker/domain/model/habit.dart';
import 'package:flutterhabittracker/domain/model/habit_fulfillment_report.dart';

import 'latest_done_dates_habit.dart';

class HabitFulfillmentReportUseCase {
  LatestDoneDatesHabitUseCase latestDoneDatesHabitUseCase;

  HabitFulfillmentReportUseCase(this.latestDoneDatesHabitUseCase);

  HabitFulfillmentReport getHabitFulfillmentReport(Habit habit) {
    var dates = latestDoneDatesHabitUseCase.getDoneDatesForLastDays(
        habit, habit.periodicity.daysNumber);
    var difference = habit.periodicity.repetitionsNumber - dates.length;

    return HabitFulfillmentReport(
        habitType: habit.type, difference: difference);
  }
}
