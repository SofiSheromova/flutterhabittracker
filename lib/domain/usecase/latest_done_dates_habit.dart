import 'package:flutterhabittracker/domain/model/habit.dart';

class LatestDoneDatesHabitUseCase {
  List<DateTime> getDoneDatesForLastDays(Habit habit, int numberOfDays) {
    var startDate = DateTime.now().add(Duration(days: -numberOfDays));
    return habit.doneDates.where((date) => date.isAfter(startDate)).toList();
  }
}
