import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutterhabittracker/presentation/editor/view/editor_page.dart';
import 'package:flutterhabittracker/presentation/habits_tabs/view/habits_page.dart';
import 'package:flutterhabittracker/presentation/info/view/info_page.dart';

import 'generated/l10n.dart';

class Routes {
  static const String home = HabitsPage.routeName;
  static const String info = InfoPage.routeName;
  static const String editor = EditorPage.routeName;
}

class AppRouter {
  AppRouter();

  Route? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case HabitsPage.routeName:
        return _homeRouteFactory(settings);

      case EditorPage.routeName:
        return _editorRouteFactory(settings);

      case InfoPage.routeName:
        return _infoRouteFactory(settings);

      default:
        assert(false, 'Need to implement ${settings.name}');
        return null;
    }
  }

  Route _homeRouteFactory(RouteSettings settings) {
    return MaterialPageRoute(
        builder: (context) => HabitsPage(
              title: S.of(context).app_bar_title_home,
            ));
  }

  Route _editorRouteFactory(RouteSettings settings) {
    final args = settings.arguments as EditorArguments;
    return MaterialPageRoute(
      builder: (context) => EditorPage(
        title: S.of(context).app_bar_title_editor,
        habit: args.habit,
      ),
    );
  }

  Route _infoRouteFactory(RouteSettings settings) {
    return MaterialPageRoute(
        builder: (context) => InfoPage(
              title: S.of(context).app_bar_title_info,
            ));
  }
}
