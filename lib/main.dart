import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutterhabittracker/presentation/app.dart';
import 'package:flutterhabittracker/routes.dart';

void main() {
  runApp(HabitTrackerApp(
    router: AppRouter(),
  ));
}
