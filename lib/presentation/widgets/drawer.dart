import 'package:flutter/material.dart';
import 'package:flutterhabittracker/generated/l10n.dart';
import 'package:flutterhabittracker/presentation/resources/values/app_colors.dart';
import 'package:flutterhabittracker/presentation/resources/values/app_dimensions.dart';

import '../../routes.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          AppDrawerHeader(),
          AppDrawerItem(
            icon: Icons.home,
            text: S.of(context).drawer_item_home,
            onTap: () {
              Navigator.of(context).pushReplacementNamed(Routes.home);
            },
          ),
          AppDrawerItem(
            icon: Icons.info,
            text: S.of(context).drawer_item_info,
            onTap: () {
              Navigator.of(context).pushReplacementNamed(Routes.info);
            },
          ),
          Divider(),
          ListTile(
            title: Text(S.of(context).drawer_item_version),
            onTap: () {},
          ),
        ],
      ),
    );
  }
}

class AppDrawerHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.secondary,
        ),
        child: Stack(children: <Widget>[
          Positioned(
              bottom: AppDimensions.DRAWER_HEADER_MARGIN,
              left: AppDimensions.DRAWER_HEADER_MARGIN,
              child: Text(S.of(context).drawer_header_title,
                  style: Theme.of(context)
                      .textTheme
                      .headline4!
                      .copyWith(color: AppColors.ON_SECONDARY))),
        ]));
  }
}

class AppDrawerItem extends StatelessWidget {
  final IconData icon;
  final String text;
  final GestureTapCallback onTap;

  const AppDrawerItem(
      {required this.icon, required this.text, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Icon(icon),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: AppDimensions.DRAWER_ITEM_PADDING),
            child: Text(text),
          )
        ],
      ),
      onTap: onTap,
    );
  }
}
