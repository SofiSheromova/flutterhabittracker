import 'package:flutter/material.dart';
import 'package:flutterhabittracker/presentation/widgets/drawer.dart';

class InfoPage extends StatelessWidget {
  static const String routeName = '/info';

  final String title;

  const InfoPage({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        drawer: AppDrawer(),
        body: Center(child: Text("Info page")));
  }
}
