import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutterhabittracker/data/database/app_database.dart';
import 'package:flutterhabittracker/data/repository.dart';
import 'package:flutterhabittracker/domain/usecase/habit_fulfillment_report.dart';
import 'package:flutterhabittracker/domain/usecase/latest_done_dates_habit.dart';
import 'package:flutterhabittracker/generated/l10n.dart';
import 'package:flutterhabittracker/presentation/themes.dart';
import 'package:flutterhabittracker/routes.dart';
import 'package:provider/provider.dart';

class HabitTrackerApp extends StatelessWidget {
  final AppRouter router;

  const HabitTrackerApp({Key? key, required this.router}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AdaptiveTheme(
      light: lightTheme,
      initial: AdaptiveThemeMode.light,
      builder: (ThemeData light, ThemeData dark) => RepositoryProvider(
        create: _createRepository,
        child: Provider(
          create: (context) =>
              HabitFulfillmentReportUseCase(LatestDoneDatesHabitUseCase()),
          child: MaterialApp(
            theme: light,
            onGenerateRoute: router.generateRoute,
            localizationsDelegates: [
              S.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: S.delegate.supportedLocales,
          ),
        ),
      ),
    );
  }

  Repository _createRepository(BuildContext context) {
    var database = AppDatabase();
    return Repository(
        habitDao: database.habitDao, requestDao: database.requestDao);
  }
}
