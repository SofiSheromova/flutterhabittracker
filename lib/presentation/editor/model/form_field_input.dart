import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class FormFieldInputModel extends Equatable {
  final String? value;
  final String? errorText;

  FormFieldInputModel({this.value, this.errorText});

  bool get isValid => value != null && errorText == null;

  @override
  List<Object?> get props => [value, errorText];
}
