import 'dart:math';

class ColorsGenerator {
  static List<int> _colors = [
    0xfff7a163,
    0xffc3e663,
    0xff63d4b6,
    0xff6392d4,
    0xff9f5dc2,
    0xffc761a5,
    0xff9c27b0
  ];
  static Random _random = new Random();

  static int generate() {
    return _colors[_random.nextInt(_colors.length)];
  }
}
