import 'package:flutterhabittracker/generated/l10n.dart';

import 'form_field_input.dart';

class DescriptionInputModel extends FormFieldInputModel {
  static const int MAX_LENGTH = 140;

  DescriptionInputModel({String? value, String? errorText})
      : super(value: value, errorText: errorText);

  factory DescriptionInputModel.validator(String? value) {
    if (value == null) return DescriptionInputModel();
    if (value.isEmpty) {
      return DescriptionInputModel(
          value: value,
          errorText: S.current.editor_validation_empty_field_error);
    }
    if (value.length > MAX_LENGTH) {
      return DescriptionInputModel(
          value: value, errorText: S.current.editor_validation_to_long_error);
    }
    return DescriptionInputModel(value: value);
  }
}
