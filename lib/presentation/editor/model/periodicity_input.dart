import 'package:flutterhabittracker/generated/l10n.dart';

import 'form_field_input.dart';

class PeriodicityInputModel extends FormFieldInputModel {
  static const int MAX_VALUE = 999;

  PeriodicityInputModel({String? value, String? errorText})
      : super(value: value, errorText: errorText);

  factory PeriodicityInputModel.validator(String? input) {
    if (input == null) return PeriodicityInputModel();
    try {
      var value = int.parse(input);
      if (value < 0) {
        return PeriodicityInputModel(
            errorText: S.current.editor_validation_invalid_value);
      }
      if (value > MAX_VALUE) {
        return PeriodicityInputModel(
            errorText: S.current.editor_validation_too_large_number);
      }

      return PeriodicityInputModel(value: input);
    } catch (error) {
      return PeriodicityInputModel(
          errorText: S.current.editor_validation_invalid_value);
    }
  }
}
