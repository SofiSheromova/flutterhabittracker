import 'package:flutterhabittracker/generated/l10n.dart';

import 'form_field_input.dart';

class TitleInputModel extends FormFieldInputModel {
  static const int MAX_LENGTH = 50;

  TitleInputModel({String? value, String? errorText})
      : super(value: value, errorText: errorText);

  factory TitleInputModel.validator(String? value) {
    if (value == null) return TitleInputModel();
    if (value.isEmpty) {
      return TitleInputModel(
          value: value,
          errorText: S.current.editor_validation_empty_field_error);
    }
    if (value.length > MAX_LENGTH) {
      return TitleInputModel(
          value: value, errorText: S.current.editor_validation_to_long_error);
    }
    return TitleInputModel(value: value);
  }
}
