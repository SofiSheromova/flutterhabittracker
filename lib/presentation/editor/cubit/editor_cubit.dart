import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterhabittracker/data/repository.dart';
import 'package:flutterhabittracker/domain/model/habit.dart';
import 'package:flutterhabittracker/domain/model/habit_priority.dart';
import 'package:flutterhabittracker/domain/model/habit_type.dart';
import 'package:flutterhabittracker/domain/model/periodicity.dart';
import 'package:flutterhabittracker/presentation/editor/model/colors_generator.dart';
import 'package:flutterhabittracker/presentation/editor/model/description_input.dart';
import 'package:flutterhabittracker/presentation/editor/model/periodicity_input.dart';
import 'package:flutterhabittracker/presentation/editor/model/title_input.dart';
import 'package:meta/meta.dart';

part 'editor_state.dart';

class EditorCubit extends Cubit<EditorState> {
  final Repository _repository;
  final Habit? _originalHabit;

  bool get habitExists => _originalHabit != null;

  EditorCubit({required Repository repository, Habit? habit})
      : this._repository = repository,
        this._originalHabit = habit,
        super(EditorState.fromHabit(habit: habit ?? Habit.blank()));

  void titleChanged(String newTitle) {
    emit(state.copyWith(title: newTitle));
  }

  void descriptionChanged(String newDescription) {
    emit(state.copyWith(description: newDescription));
  }

  void habitTypeChanged(HabitType newHabitType) {
    emit(state.copyWith(habitType: newHabitType));
  }

  void habitPriorityChanged(HabitPriority newHabitPriority) {
    emit(state.copyWith(habitPriority: newHabitPriority));
  }

  void habitRepetitionsNumberChanged(String value) {
    emit(state.copyWith(repetitionsNumber: value));
  }

  void habitDaysNumberChanged(String value) {
    emit(state.copyWith(daysNumber: value));
  }

  Habit _mapStateToHabit() {
    var title = state.title;
    var description = state.description;
    var repetitionsNumber = state.repetitionsNumber;
    var daysNumber = state.daysNumber;

    if (!state.isValid ||
        title == null ||
        description == null ||
        repetitionsNumber == null ||
        daysNumber == null) {
      throw ArgumentError("Incorrect state data to map");
    }

    return Habit(
      title: title,
      description: description,
      type: state.habitType,
      priority: state.habitPriority,
      periodicity: Periodicity(
          repetitionsNumber: int.parse(repetitionsNumber),
          daysNumber: int.parse(daysNumber)),
      color: ColorsGenerator.generate(),
    );
  }

  void saveHabit() {
    var habit = _mapStateToHabit();

    emit(state.copyWith(status: EditorStatus.submissionInProgress));

    Future saveFuture;
    if (habitExists) {
      saveFuture = _repository.updateHabit(_originalHabit!.id, habit);
    } else {
      saveFuture = _repository.insertHabit(habit);
    }

    saveFuture.then((item) {
      if (item != null)
        emit(state.copyWith(status: EditorStatus.submissionSuccess));
      else
        emit(state.copyWith(status: EditorStatus.submissionFailure));
    }).onError((error, stackTrace) {
      print(error);
      emit(state.copyWith(status: EditorStatus.submissionFailure));
    });
  }

  void deleteHabit() {
    if (habitExists) {
      _repository.deleteHabit(_originalHabit!).then((value) {
        if (value)
          emit(state.copyWith(status: EditorStatus.submissionSuccess));
        else
          emit(state.copyWith(status: EditorStatus.submissionFailure));
      });
    }
  }
}
