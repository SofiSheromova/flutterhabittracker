part of 'editor_cubit.dart';

enum EditorStatus {
  initial,
  submissionInProgress,
  submissionSuccess,
  submissionFailure
}

@immutable
class EditorState extends Equatable {
  final TitleInputModel titleInput;
  final DescriptionInputModel descriptionInput;
  final HabitType habitType;
  final HabitPriority habitPriority;
  final PeriodicityInputModel repetitionsInput;
  final PeriodicityInputModel daysInput;
  final EditorStatus status;

  String? get title => titleInput.value;

  String? get description => descriptionInput.value;

  String? get repetitionsNumber => repetitionsInput.value;

  String? get daysNumber => daysInput.value;

  bool get isValid =>
      titleInput.isValid &&
      descriptionInput.isValid &&
      repetitionsInput.isValid &&
      daysInput.isValid;

  EditorState(
      {String? title,
      String? description,
      required this.habitType,
      required this.habitPriority,
      required Periodicity periodicity,
      this.status = EditorStatus.initial})
      : this.titleInput = TitleInputModel.validator(title),
        this.descriptionInput = DescriptionInputModel.validator(description),
        this.repetitionsInput = PeriodicityInputModel.validator(
            periodicity.repetitionsNumber.toString()),
        this.daysInput =
            PeriodicityInputModel.validator(periodicity.daysNumber.toString());

  EditorState.fromHabit({required Habit habit})
      : this(
            title: habit.title.isEmpty ? null : habit.title,
            description: habit.description.isEmpty ? null : habit.description,
            habitType: habit.type,
            habitPriority: habit.priority,
            periodicity: habit.periodicity);

  EditorState._inputs(
      {required this.titleInput,
      required this.descriptionInput,
      required this.habitType,
      required this.habitPriority,
      required this.repetitionsInput,
      required this.daysInput,
      required this.status});

  EditorState copyWith(
      {String? title,
      String? description,
      EditorStatus? status,
      HabitType? habitType,
      HabitPriority? habitPriority,
      String? repetitionsNumber,
      String? daysNumber}) {
    var newTitleInput = title != null ? TitleInputModel.validator(title) : null;

    var newDescriptionInput = description != null
        ? DescriptionInputModel.validator(description)
        : null;

    var newRepetitionsInput = repetitionsNumber != null
        ? PeriodicityInputModel.validator(repetitionsNumber)
        : null;

    var newDaysInput =
        daysNumber != null ? PeriodicityInputModel.validator(daysNumber) : null;

    return EditorState._inputs(
      titleInput: newTitleInput ?? this.titleInput,
      descriptionInput: newDescriptionInput ?? this.descriptionInput,
      habitType: habitType ?? this.habitType,
      habitPriority: habitPriority ?? this.habitPriority,
      repetitionsInput: newRepetitionsInput ?? this.repetitionsInput,
      daysInput: newDaysInput ?? this.daysInput,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props => [
        titleInput,
        descriptionInput,
        habitType,
        habitPriority,
        repetitionsInput,
        daysInput,
        status,
      ];
}
