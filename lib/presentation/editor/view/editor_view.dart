import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterhabittracker/generated/l10n.dart';
import 'package:flutterhabittracker/presentation/editor/cubit/editor_cubit.dart';
import 'package:flutterhabittracker/presentation/resources/values/app_dimensions.dart';

import 'editor_form/form.dart';

class EditorView extends StatelessWidget {
  const EditorView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<EditorCubit, EditorState>(
      listener: (context, state) {
        if (state.status == EditorStatus.submissionSuccess) {
          Navigator.pop(context);
        } else if (state.status == EditorStatus.submissionFailure) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(S.of(context).editor_habit_save_error_msg)));
        }
      },
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(AppDimensions.EDITOR_FORM_PADDING),
          child: EditorForm(),
        ),
      ),
    );
  }
}
