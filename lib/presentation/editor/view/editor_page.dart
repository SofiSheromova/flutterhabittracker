import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterhabittracker/data/repository.dart';
import 'package:flutterhabittracker/domain/model/habit.dart';
import 'package:flutterhabittracker/presentation/editor/cubit/editor_cubit.dart';

import 'editor_view.dart';

class EditorPage extends StatelessWidget {
  static const String routeName = '/editor';

  final String title;
  final Habit? habit;

  EditorPage({Key? key, required this.title, this.habit}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var repository = context.read<Repository>();

    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: BlocProvider(
        create: (_) => EditorCubit(repository: repository, habit: habit),
        child: EditorView(),
      ),
    );
  }
}

class EditorArguments {
  final Habit? habit;

  EditorArguments({this.habit});
}
