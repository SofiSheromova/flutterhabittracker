import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterhabittracker/generated/l10n.dart';
import 'package:flutterhabittracker/presentation/editor/cubit/editor_cubit.dart';

class SaveButton extends StatelessWidget {
  const SaveButton();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditorCubit, EditorState>(
      builder: (context, state) {
        return state.status == EditorStatus.submissionInProgress
            ? const CircularProgressIndicator()
            : ElevatedButton(
                onPressed: state.isValid
                    ? () => context.read<EditorCubit>().saveHabit()
                    : null,
                child: Text(S.of(context).editor_save_btn),
              );
      },
    );
  }
}
