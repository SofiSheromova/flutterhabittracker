import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterhabittracker/generated/l10n.dart';
import 'package:flutterhabittracker/presentation/editor/cubit/editor_cubit.dart';

class PeriodicityTextField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var editorCubit = context.read<EditorCubit>();

    return Row(
      children: [
        Flexible(
            child: BlocBuilder<EditorCubit, EditorState>(
          builder: (context, state) => TextFormField(
            initialValue: state.repetitionsNumber,
            keyboardType: TextInputType.number,
            textAlign: TextAlign.center,
            decoration:
                InputDecoration(errorText: state.repetitionsInput.errorText),
            onChanged: (String value) =>
                editorCubit.habitRepetitionsNumberChanged(value),
          ),
        )),
        Text(S.of(context).editor_form_periodicity_repetitions_text),
        Flexible(
            child: BlocBuilder<EditorCubit, EditorState>(
          builder: (context, state) => TextFormField(
            initialValue: state.daysNumber,
            keyboardType: TextInputType.number,
            textAlign: TextAlign.center,
            decoration: InputDecoration(errorText: state.daysInput.errorText),
            onChanged: (String value) =>
                editorCubit.habitDaysNumberChanged(value),
          ),
        )),
        Text(S.of(context).editor_form_periodicity_days_text)
      ],
    );
  }
}
