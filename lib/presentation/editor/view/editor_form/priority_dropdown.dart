import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterhabittracker/domain/model/habit_priority.dart';
import 'package:flutterhabittracker/generated/l10n.dart';
import 'package:flutterhabittracker/presentation/editor/cubit/editor_cubit.dart';

class PriorityDropdownButton extends StatelessWidget {
  String _priorityName(HabitPriority priority, BuildContext context) {
    switch (priority) {
      case HabitPriority.low:
        return S.of(context).habit_priority_low;
      case HabitPriority.middle:
        return S.of(context).habit_priority_middle;
      case HabitPriority.high:
        return S.of(context).habit_priority_high;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditorCubit, EditorState>(
      buildWhen: (previous, current) =>
          previous.habitPriority != current.habitPriority,
      builder: (context, state) {
        return DropdownButton<HabitPriority>(
          items: HabitPriority.values
              .map((HabitPriority value) => DropdownMenuItem(
                    value: value,
                    child: Text(_priorityName(value, context)),
                  ))
              .toList(),
          onChanged: (HabitPriority? value) {
            if (value == null) return;
            context.read<EditorCubit>().habitPriorityChanged(value);
          },
          value: state.habitPriority,
        );
      },
    );
  }
}
