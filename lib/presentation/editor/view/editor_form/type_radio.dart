import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterhabittracker/domain/model/habit_type.dart';
import 'package:flutterhabittracker/generated/l10n.dart';
import 'package:flutterhabittracker/presentation/editor/cubit/editor_cubit.dart';

class TypeRadioGroup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditorCubit, EditorState>(
      buildWhen: (previous, current) => previous.habitType != current.habitType,
      builder: (context, state) => Column(
        children: [
          RadioListTile(
            title: Text(S.of(context).editor_form_habit_type_good),
            value: HabitType.good,
            groupValue: state.habitType,
            onChanged: (value) => setSelectedRadio(context, value),
          ),
          RadioListTile(
            title: Text(S.of(context).editor_form_habit_type_bad),
            value: HabitType.bad,
            groupValue: state.habitType,
            onChanged: (value) => setSelectedRadio(context, value),
          )
        ],
      ),
    );
  }

  setSelectedRadio(BuildContext context, Object? value) {
    if (value == null) return;
    context.read<EditorCubit>().habitTypeChanged(value as HabitType);
  }
}
