import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterhabittracker/generated/l10n.dart';
import 'package:flutterhabittracker/presentation/editor/cubit/editor_cubit.dart';

class DeleteButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var editorCubit = context.read<EditorCubit>();
    return ElevatedButton(
      onPressed: editorCubit.habitExists ? editorCubit.deleteHabit : null,
      child: Text(S.of(context).editor_delete_btn),
    );
  }
}
