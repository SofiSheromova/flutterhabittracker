import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterhabittracker/presentation/editor/cubit/editor_cubit.dart';

class TitleTextField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditorCubit, EditorState>(
      buildWhen: (previous, current) => previous.title != current.title,
      builder: (context, state) {
        return TextFormField(
          initialValue: state.title,
          onChanged: (title) => context.read<EditorCubit>().titleChanged(title),
          decoration: InputDecoration(
            errorText: state.titleInput.errorText,
          ),
        );
      },
    );
  }
}
