import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterhabittracker/generated/l10n.dart';
import 'package:flutterhabittracker/presentation/resources/values/app_dimensions.dart';

import 'delete_button.dart';
import 'description_field.dart';
import 'perioicity_field.dart';
import 'priority_dropdown.dart';
import 'save_button.dart';
import 'title_field.dart';
import 'type_radio.dart';

class EditorForm extends StatelessWidget {
  const EditorForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(S.of(context).editor_form_title_header),
          TitleTextField(),
          _ItemSpacer(),
          Text(S.of(context).editor_form_description_header),
          DescriptionTextField(),
          _ItemSpacer(),
          Text(S.of(context).editor_form_type_header),
          TypeRadioGroup(),
          _ItemSpacer(),
          Text(S.of(context).editor_form_priority_header),
          PriorityDropdownButton(),
          _ItemSpacer(),
          Text(S.of(context).editor_form_periodicity_header),
          PeriodicityTextField(),
          _ItemSpacer(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [DeleteButton(), SaveButton()],
          ),
        ]);
  }
}

class _ItemSpacer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: AppDimensions.EDITOR_FORM_SPACE_BETWEEN_LINE,
        width: AppDimensions.EDITOR_FORM_SPACE_BETWEEN_LINE);
  }
}
