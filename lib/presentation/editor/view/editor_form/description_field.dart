import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterhabittracker/presentation/editor/cubit/editor_cubit.dart';

class DescriptionTextField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditorCubit, EditorState>(
      buildWhen: (previous, current) =>
          previous.descriptionInput != current.descriptionInput,
      builder: (context, state) {
        return TextFormField(
          initialValue: state.description,
          onChanged: (String description) =>
              context.read<EditorCubit>().descriptionChanged(description),
          decoration: InputDecoration(
            errorText: state.descriptionInput.errorText,
          ),
        );
      },
    );
  }
}
