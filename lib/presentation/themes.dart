import 'package:flutter/material.dart';
import 'package:flutterhabittracker/presentation/resources/values/app_colors.dart';

final textTheme = ThemeData.light().textTheme;

final lightTheme = ThemeData.light().copyWith(
  textTheme: textTheme.copyWith(
    subtitle2: textTheme.subtitle2!.copyWith(
        fontWeight: FontWeight.w400, color: Colors.black45, fontSize: 16),
  ),
  appBarTheme: AppBarTheme().copyWith(
    backgroundColor: AppColors.PRIMARY_COLOR,
  ),
  colorScheme: ColorScheme.light().copyWith(
    primary: AppColors.PRIMARY_COLOR,
    primaryVariant: AppColors.PRIMARY_VARIANT_COLOR,
    onPrimary: AppColors.ON_PRIMARY,
    secondary: AppColors.SECONDARY_COLOR,
    secondaryVariant: AppColors.PRIMARY_VARIANT_COLOR,
    onSecondary: AppColors.ON_SECONDARY,
  ),
  accentColor: AppColors.SECONDARY_COLOR,
  primaryColor: AppColors.PRIMARY_COLOR,
  primaryColorLight: AppColors.PRIMARY_COLOR_LIGHT,
  primaryColorDark: AppColors.PRIMARY_COLOR_DARK,
);
