part of 'filters_cubit.dart';

@immutable
class FiltersState extends Equatable {
  final String? searchQuery;
  final HabitsComparator? sortFunction;
  final int? checkedButtonIndex;

  FiltersState({this.searchQuery, this.sortFunction, this.checkedButtonIndex});

  FiltersState copyWith({
    String? searchBarData,
    HabitsComparator? comparator,
    int? checkedButtonIndex,
  }) {
    return FiltersState(
      searchQuery: searchBarData ?? this.searchQuery,
      sortFunction: comparator ?? this.sortFunction,
      checkedButtonIndex: checkedButtonIndex ?? this.checkedButtonIndex,
    );
  }

  @override
  List<Object?> get props => [searchQuery, sortFunction, checkedButtonIndex];
}
