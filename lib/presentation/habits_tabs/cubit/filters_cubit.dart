import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterhabittracker/domain/model/habit.dart';
import 'package:flutterhabittracker/presentation/habits_tabs/cubit/habits_comparators.dart';

part 'filters_state.dart';

class FiltersCubit extends Cubit<FiltersState> {
  FiltersCubit() : super(FiltersState());

  void searchQueryChanged(String text) {
    emit(state.copyWith(searchBarData: text));
  }

  void sortByDate({int? checkedButtonIndex}) {
    emit(state.copyWith(
      comparator: dateComparator,
      checkedButtonIndex: checkedButtonIndex,
    ));
  }

  void sortInverseByDate({int? checkedButtonIndex}) {
    emit(state.copyWith(
      comparator: dateComparatorReversed,
      checkedButtonIndex: checkedButtonIndex,
    ));
  }

  List<Habit> sortAndSelectHabits(List<Habit> allHabits) {
    var habits = [...allHabits];

    final sortedFunction = state.sortFunction;
    if (sortedFunction != null) habits.sort(sortedFunction);

    final searchQuery = state.searchQuery;
    if (searchQuery != null)
      habits = habits
          .where((element) => element.title.contains(searchQuery))
          .toList();

    return habits;
  }
}
