import 'dart:core';

import 'package:flutterhabittracker/domain/model/habit.dart';

typedef HabitsComparator = int Function(Habit first, Habit second);

final HabitsComparator dateComparator =
    (Habit first, Habit second) => first.date.compareTo(second.date);

final HabitsComparator dateComparatorReversed =
    (Habit first, Habit second) => second.date.compareTo(first.date);
