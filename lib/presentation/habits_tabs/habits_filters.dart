import 'package:flutterhabittracker/domain/model/habit.dart';
import 'package:flutterhabittracker/domain/model/habit_type.dart';

typedef HabitsFilter = bool Function(Habit);

final HabitsFilter goodHabitsFilter =
    (Habit habit) => habit.type == HabitType.good;
final HabitsFilter badHabitsFilter =
    (Habit habit) => habit.type == HabitType.bad;
