import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterhabittracker/generated/l10n.dart';
import 'package:flutterhabittracker/presentation/editor/view/editor_page.dart';
import 'package:flutterhabittracker/presentation/habits_list/view/list_view.dart';
import 'package:flutterhabittracker/presentation/habits_tabs/cubit/filters_cubit.dart';
import 'package:flutterhabittracker/presentation/resources/values/app_dimensions.dart';
import 'package:flutterhabittracker/presentation/widgets/drawer.dart';
import 'package:flutterhabittracker/routes.dart';

import '../habits_filters.dart';
import 'filters_bottom_sheet.dart';

class HabitsPage extends StatelessWidget {
  static const String routeName = '/';

  final String title;

  HabitsPage({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => FiltersCubit(),
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text(title),
            bottom: TabBar(
              indicatorColor: Theme.of(context).primaryColorDark,
              tabs: [
                Tab(text: S.of(context).habits_list_good_habits_tab),
                Tab(text: S.of(context).habits_list_bad_habits_tab),
              ],
            ),
          ),
          drawer: AppDrawer(),
          body: TabBarView(
            children: [
              HabitsListView(
                habitsFilter: goodHabitsFilter,
              ),
              HabitsListView(
                habitsFilter: badHabitsFilter,
              ),
            ],
          ),
          floatingActionButton: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                margin: EdgeInsets.symmetric(
                    vertical: AppDimensions.HABITS_LIST_FAB_MARGIN),
                child: FloatingActionButton(
                  heroTag: "creating_habits_button",
                  child: const Icon(Icons.add),
                  tooltip: S.of(context).fab_habit_creation_tooltip,
                  onPressed: () {
                    Navigator.of(context)
                        .pushNamed(Routes.editor, arguments: EditorArguments());
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(
                    vertical: AppDimensions.HABITS_LIST_FAB_MARGIN),
                child: BlocBuilder<FiltersCubit, FiltersState>(
                  builder: (context, state) => FloatingActionButton(
                    heroTag: "filter_button",
                    child: const Icon(Icons.filter_alt),
                    onPressed: () {
                      var filtersCubit = context.read<FiltersCubit>();
                      showModalBottomSheet(
                        context: context,
                        builder: (context) => BlocProvider.value(
                          value: filtersCubit,
                          child: FiltersBottomSheet(),
                        ),
                        isScrollControlled: true,
                        backgroundColor: Colors.transparent,
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
