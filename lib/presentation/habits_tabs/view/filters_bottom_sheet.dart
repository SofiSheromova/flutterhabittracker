import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterhabittracker/generated/l10n.dart';
import 'package:flutterhabittracker/presentation/habits_tabs/cubit/filters_cubit.dart';
import 'package:flutterhabittracker/presentation/resources/values/app_dimensions.dart';

class FiltersBottomSheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Wrap(children: [
      Container(
        padding: EdgeInsets.all(AppDimensions.BOTTOM_SHEET_PADDING),
        decoration: BoxDecoration(
          color: Theme.of(context).canvasColor,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(AppDimensions.BOTTOM_SHEET_BORDER_RADIUS),
            topRight: Radius.circular(AppDimensions.BOTTOM_SHEET_BORDER_RADIUS),
          ),
        ),
        child: Center(
          child: Column(children: [
            Row(children: [
              Expanded(child: Text(S.of(context).habits_list_sort_by_date)),
              BlocBuilder<FiltersCubit, FiltersState>(
                buildWhen: (previous, current) =>
                    previous.checkedButtonIndex != current.checkedButtonIndex,
                builder: (context, state) => HabitsSortRadioGroup(state),
              ),
            ]),
            _ItemSpacer(),
            HabitsSearchTextField(),
            SizedBox(height: MediaQuery.of(context).viewInsets.bottom)
          ]),
        ),
      ),
    ]);
  }
}

class HabitsSortRadioGroup extends StatelessWidget {
  final FiltersState state;

  HabitsSortRadioGroup(this.state);

  @override
  Widget build(BuildContext context) {
    var cubit = context.read<FiltersCubit>();
    final List<HabitsSortItemModel> items = [
      HabitsSortItemModel(false, Icons.keyboard_arrow_down, cubit.sortByDate),
      HabitsSortItemModel(
          false, Icons.keyboard_arrow_up, cubit.sortInverseByDate),
    ];
    var checkedIndex = state.checkedButtonIndex;
    if (checkedIndex != null) {
      items[checkedIndex].isSelected = true;
    }

    return Container(
      height: AppDimensions.HABITS_LIST_SORT_BUTTON_HEIGHT,
      width: AppDimensions.HABITS_LIST_SORT_BUTTON_WIDTH * 2 +
          AppDimensions.HABITS_LIST_SORT_BUTTON_MARGIN * 4,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: items.length,
          itemBuilder: (BuildContext context, int index) {
            return new InkWell(
              onTap: () => items[index].sortFunction(checkedButtonIndex: index),
              child: HabitsSortRadioItem(items[index]),
            );
          }),
    );
  }
}

class HabitsSortRadioItem extends StatelessWidget {
  final HabitsSortItemModel _item;

  HabitsSortRadioItem(this._item);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: AppDimensions.HABITS_LIST_SORT_BUTTON_WIDTH,
      margin: EdgeInsets.symmetric(
          horizontal: AppDimensions.HABITS_LIST_SORT_BUTTON_MARGIN),
      child: Center(
        child: Icon(
          _item.icon,
          color: _item.isSelected
              ? Theme.of(context).colorScheme.onPrimary
              : Colors.black.withOpacity(0.5),
        ),
      ),
      decoration: BoxDecoration(
        color: _item.isSelected
            ? Theme.of(context).primaryColor
            : Colors.transparent,
        border: Border.all(
          color: _item.isSelected
              ? Colors.transparent
              : Colors.black.withOpacity(0.5),
        ),
        borderRadius: const BorderRadius.all(const Radius.circular(2.0)),
      ),
    );
  }
}

class HabitsSortItemModel {
  bool isSelected;
  final IconData icon;
  final Function({int? checkedButtonIndex}) sortFunction;

  HabitsSortItemModel(this.isSelected, this.icon, this.sortFunction);
}

class HabitsSearchTextField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var cubit = context.read<FiltersCubit>();
    return BlocBuilder<FiltersCubit, FiltersState>(
        builder: (context, state) => TextFormField(
              initialValue: state.searchQuery,
              onChanged: cubit.searchQueryChanged,
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.search),
                border: UnderlineInputBorder(),
                hintText: S.of(context).habits_list_search_hint,
              ),
            ));
  }
}

class _ItemSpacer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: AppDimensions.EDITOR_FORM_SPACE_BETWEEN_LINE,
        width: AppDimensions.EDITOR_FORM_SPACE_BETWEEN_LINE);
  }
}
