class AppDimensions {
  static const double DRAWER_ITEM_PADDING = 8.0;
  static const double DRAWER_HEADER_FONT_SIZE = 20.0;
  static const double DRAWER_HEADER_MARGIN = 16.0;
  static const double HABITS_LIST_ITEM_SPACE_BETWEEN_LINE = 10.0;
  static const double HABITS_LIST_ITEM_PADDING = 10.0;
  static const double HABITS_LIST_ITEM_MARGIN = 10.0;
  static const double HABITS_LIST_ITEM_BORDER_RADIUS = 10.0;
  static const double EDITOR_FORM_PADDING = 20.0;
  static const double EDITOR_FORM_SPACE_BETWEEN_LINE = 10.0;
  static const double HABITS_LIST_SORT_BUTTON_WIDTH = 50.0;
  static const double HABITS_LIST_SORT_BUTTON_HEIGHT = 40.0;
  static const double HABITS_LIST_SORT_BUTTON_MARGIN = 5.0;
  static const double HABITS_LIST_FAB_MARGIN = 5.0;
  static const double BOTTOM_SHEET_PADDING = 25.0;
  static const double BOTTOM_SHEET_BORDER_RADIUS = 25.0;
}
