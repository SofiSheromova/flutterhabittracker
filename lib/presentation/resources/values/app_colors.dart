import 'package:flutter/material.dart';

class AppColors {
  static const Color PRIMARY_COLOR = Colors.amber;
  static const Color PRIMARY_COLOR_LIGHT = Color(0xffffd54f);
  static const Color PRIMARY_COLOR_DARK = Color(0xffff8f00);
  static const Color PRIMARY_VARIANT_COLOR = Colors.amberAccent;
  static const Color ON_PRIMARY = Colors.white;
  static const Color SECONDARY_COLOR = Colors.deepPurpleAccent;
  static const Color SECONDARY_VARIANT_COLOR = Colors.purple;
  static const Color ON_SECONDARY = Colors.white;
}
