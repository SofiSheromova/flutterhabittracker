import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterhabittracker/data/repository.dart';
import 'package:flutterhabittracker/domain/model/habit.dart';
import 'package:flutterhabittracker/generated/l10n.dart';
import 'package:flutterhabittracker/presentation/habits_list/cubit/habits_list_cubit.dart';
import 'package:flutterhabittracker/presentation/habits_tabs/cubit/filters_cubit.dart';
import 'package:flutterhabittracker/presentation/habits_tabs/habits_filters.dart';
import 'package:flutterhabittracker/presentation/resources/values/app_dimensions.dart';

import 'habits_list_item/item.dart';

class HabitsListView extends StatelessWidget {
  final bool Function(Habit)? _habitsFilter;

  const HabitsListView({Key? key, HabitsFilter? habitsFilter})
      : this._habitsFilter = habitsFilter,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    var repository = context.read<Repository>();

    return Scaffold(
      body: Center(
        child: BlocProvider(
          create: (_) => HabitsListCubit(
              repository: repository, habitsFilter: _habitsFilter),
          child: BlocBuilder<HabitsListCubit, HabitsState>(
              builder: (context, state) => HabitsListContent(state: state)),
        ),
      ),
    );
  }
}

class HabitsListContent extends StatelessWidget {
  final HabitsState state;

  const HabitsListContent({Key? key, required this.state}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (state is HabitsLoading) {
      return Center(child: CircularProgressIndicator());
    }

    if (state is HabitsLoadFailure) {
      return Center(child: Text(S.of(context).habits_list_load_failure));
    }

    var habits = (state as HabitsSuccessLoaded).habits;

    if (habits.isEmpty)
      return Center(child: Text(S.of(context).habits_list_empty));

    return BlocBuilder<FiltersCubit, FiltersState>(
        builder: (context, filtersState) {
      var preparedHabits =
          context.read<FiltersCubit>().sortAndSelectHabits(habits);

      return ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          var margin = index == preparedHabits.length - 1
              ? EdgeInsets.all(AppDimensions.HABITS_LIST_ITEM_MARGIN)
              : EdgeInsets.all(AppDimensions.HABITS_LIST_ITEM_MARGIN)
                  .copyWith(bottom: 0.0);

          return Container(
            margin: margin,
            child: HabitsListItem(habit: preparedHabits[index]),
          );
        },
        itemCount: preparedHabits.length,
      );
    });
  }
}
