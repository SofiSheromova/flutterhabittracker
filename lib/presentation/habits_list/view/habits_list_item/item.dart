import 'package:flutter/material.dart';
import 'package:flutterhabittracker/domain/model/habit.dart';
import 'package:flutterhabittracker/presentation/editor/view/editor_page.dart';
import 'package:flutterhabittracker/presentation/habits_list/view/habits_list_item/periodicity.dart';
import 'package:flutterhabittracker/presentation/resources/values/app_dimensions.dart';
import 'package:flutterhabittracker/routes.dart';

import 'description.dart';
import 'done_button.dart';
import 'header.dart';

class HabitsListItem extends StatelessWidget {
  final Habit habit;

  const HabitsListItem({Key? key, required this.habit}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var color = Color(habit.color);

    return Material(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
              AppDimensions.HABITS_LIST_ITEM_BORDER_RADIUS), // if you need this
          side: BorderSide(
            color: Colors.black45.withOpacity(0.1),
            width: 1,
          ),
        ),
        child: InkWell(
          hoverColor: color.withAlpha(15),
          highlightColor: color.withAlpha(25),
          splashColor: color.withAlpha(75),
          onTap: () {
            Navigator.of(context).pushNamed(Routes.editor,
                arguments: EditorArguments(habit: habit));
          },
          child: Container(
            padding: EdgeInsets.all(AppDimensions.HABITS_LIST_ITEM_PADDING),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                HabitsListItemHeader(habit: habit),
                _ItemSpacer(),
                HabitsListItemDescription(habit: habit),
                _ItemSpacer(),
                HabitsListItemPeriodicity(
                  habit: habit,
                ),
                _ItemSpacer(),
                HabitsListItemDoneButton(habit: habit),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _ItemSpacer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: AppDimensions.HABITS_LIST_ITEM_SPACE_BETWEEN_LINE,
        width: AppDimensions.HABITS_LIST_ITEM_SPACE_BETWEEN_LINE);
  }
}
