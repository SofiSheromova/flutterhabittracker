import 'package:flutter/material.dart';
import 'package:flutterhabittracker/domain/model/habit.dart';
import 'package:flutterhabittracker/generated/l10n.dart';

class HabitsListItemPeriodicity extends StatelessWidget {
  final Habit habit;

  const HabitsListItemPeriodicity({Key? key, required this.habit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final periodicity = habit.periodicity;

    return Row(
      children: [
        Icon(Icons.repeat, color: Color(habit.color)),
        Text(
          S.of(context).habits_list_item_repetitions_number(
              periodicity.repetitionsNumber),
          style: Theme.of(context).textTheme.subtitle1,
        ),
        Text(
          S.of(context).habits_list_item_days_number(periodicity.daysNumber),
          style: Theme.of(context).textTheme.subtitle1,
        ),
      ],
    );
  }
}
