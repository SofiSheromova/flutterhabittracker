import 'package:flutter/material.dart';
import 'package:flutterhabittracker/domain/model/habit.dart';
import 'package:flutterhabittracker/domain/usecase/habit_fulfillment_report.dart';
import 'package:flutterhabittracker/generated/l10n.dart';
import 'package:flutterhabittracker/presentation/habits_list/cubit/habits_list_cubit.dart';
import 'package:provider/provider.dart';

import '../../model/report_formatter.dart';


class HabitsListItemDoneButton extends StatelessWidget {
  final Habit habit;

  const HabitsListItemDoneButton({Key? key, required this.habit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var cubit = context.read<HabitsListCubit>();

    return Container(
      width: double.infinity,
      child: ElevatedButton(
        onPressed: () {
          cubit
              .markHabitDone(habit)
              .then((markedHabit) => showReport(context, markedHabit));
        },
        child: Text(S.of(context).habits_list_item_done_btn),
        style: ElevatedButton.styleFrom(primary: Color(habit.color)),
      ),
    );
  }

  void showReport(BuildContext context, Habit habit) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      backgroundColor: Theme.of(context).accentColor,
      duration: Duration(seconds: 2),
      content: Text(getHabitReport(context, habit)),
    ));
  }

  String getHabitReport(BuildContext context, Habit habit) => context
      .read<HabitFulfillmentReportUseCase>()
      .getHabitFulfillmentReport(habit)
      .reportToString(context);
}
