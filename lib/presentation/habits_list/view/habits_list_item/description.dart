import 'package:flutter/material.dart';
import 'package:flutterhabittracker/domain/model/habit.dart';


class HabitsListItemDescription extends StatelessWidget {
  final Habit habit;

  const HabitsListItemDescription({Key? key, required this.habit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(habit.description,
        style: Theme.of(context).textTheme.subtitle2);
  }
}
