import 'package:flutter/material.dart';
import 'package:flutterhabittracker/domain/model/habit.dart';
import 'package:flutterhabittracker/domain/model/habit_priority.dart';
import 'package:flutterhabittracker/presentation/resources/values/app_dimensions.dart';

class HabitsListItemHeader extends StatelessWidget {
  final Habit habit;

  const HabitsListItemHeader({Key? key, required this.habit}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CircleAvatar(
          foregroundColor: Theme.of(context).colorScheme.onPrimary,
          backgroundColor: Color(habit.color),
          child: Text(habit.priority.value),
        ),
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: AppDimensions.HABITS_LIST_ITEM_SPACE_BETWEEN_LINE),
          child: Text(
            habit.title,
            style: Theme.of(context).textTheme.headline5,
          ),
        ),
      ],
    );
  }
}
