import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutterhabittracker/data/repository.dart';
import 'package:flutterhabittracker/domain/model/habit.dart';
import 'package:meta/meta.dart';

part 'habits_list_state.dart';

class HabitsListCubit extends Cubit<HabitsState> {
  final Repository repository;
  Stream<List<Habit>> _habitsStream;
  bool Function(Habit)? _habitsFilter;

  HabitsListCubit(
      {required this.repository, bool Function(Habit)? habitsFilter})
      : _habitsStream = repository.watchAllHabits(),
        _habitsFilter = habitsFilter,
        super(HabitsLoading()) {
    _habitsStream.listen((allHabits) {
      emit(HabitsSuccessLoaded(habits: filterHabits(allHabits)));
    }, onError: (error, stackTrace) {
      print(error);
      emit(HabitsLoadFailure());
    });
  }

  List<Habit> filterHabits(List<Habit> allHabits) {
    return _habitsFilter != null
        ? allHabits.where(_habitsFilter!).toList()
        : allHabits;
  }

  Future<Habit> markHabitDone(Habit habit) {
    return repository.markHabitDone(habit, DateTime.now());
  }
}
