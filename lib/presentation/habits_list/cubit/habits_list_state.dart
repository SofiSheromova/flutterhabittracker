part of 'habits_list_cubit.dart';

enum ListStatus { loading, success, failure }

@immutable
abstract class HabitsState {}

class HabitsLoading extends HabitsState {}

class HabitsLoadFailure extends HabitsState {}

class HabitsSuccessLoaded extends HabitsState {
  final List<Habit> habits;

  HabitsSuccessLoaded({required this.habits});
}
