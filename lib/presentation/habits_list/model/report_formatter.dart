import 'package:flutter/cupertino.dart';
import 'package:flutterhabittracker/domain/model/habit_fulfillment_report.dart';
import 'package:flutterhabittracker/domain/model/habit_type.dart';
import 'package:flutterhabittracker/generated/l10n.dart';

extension ReportFormatter on HabitFulfillmentReport {
  String reportToString(BuildContext context) {
    switch (this.habitType) {
      case HabitType.good:
        if (this.difference <= 0)
          return S.of(context).habit_fulfillment_report_breathtaking;
        return S
            .of(context)
            .habit_fulfillment_report_still_need_to_be_done(this.difference);
      case HabitType.bad:
        if (this.difference <= 0)
          return S.of(context).habit_fulfillment_report_stop;
        return S
            .of(context)
            .habit_fulfillment_report_can_be_done(this.difference);
    }
  }
}
