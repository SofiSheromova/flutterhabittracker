import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'app_database.g.dart';

@DataClassName("HabitEntity")
class Habits extends Table {
  TextColumn get uid => text()();

  TextColumn get title => text().withLength(min: 1, max: 50)();

  TextColumn get description => text().withLength(min: 1, max: 140)();

  IntColumn get priority => integer()();

  IntColumn get type => integer()();

  IntColumn get count => integer()();

  IntColumn get frequency => integer()();

  IntColumn get color => integer()();

  DateTimeColumn get date => dateTime()();

  TextColumn get doneDates => text().map(DateListConverter())();

  @override
  Set<Column> get primaryKey => {uid};
}

class DateListConverter extends TypeConverter<List<DateTime>, String> {
  @override
  List<DateTime>? mapToDart(String? fromDb) {
    if (fromDb == null) return null;
    return fromDb
        .split(',')
        .where((element) => element.isNotEmpty)
        .map((element) => int.parse(element))
        .map((ms) => DateTime.fromMillisecondsSinceEpoch(ms))
        .toList();
  }

  @override
  String? mapToSql(List<DateTime>? value) {
    if (value == null) return null;
    return value
        .map((e) => e.millisecondsSinceEpoch)
        .map((e) => e.toString())
        .join(",");
  }
}

enum ActionType { insert, update, delete, markDone }

@DataClassName("RequestEntity")
class Requests extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get habitUid => text()();

  IntColumn get type => intEnum<ActionType>()();

  DateTimeColumn get extra => dateTime().nullable()();
}

@UseDao(tables: [Habits])
class HabitDao extends DatabaseAccessor<AppDatabase> with _$HabitDaoMixin {
  final AppDatabase database;

  HabitDao(this.database) : super(database);

  Future<List<HabitEntity>> get allHabits => select(habits).get();

  Stream<List<HabitEntity>> watchAllHabits() => select(habits).watch();

  Future<HabitEntity?> getHabitByUid(String uid) {
    return (select(habits)..where((t) => t.uid.equals(uid))).getSingleOrNull();
  }

  Future<HabitsCompanion?> insertHabit(HabitsCompanion entry) {
    try {
      return into(habits)
          .insert(entry, mode: InsertMode.insertOrFail)
          .then((_) => entry);
    } catch (error) {
      return Future.value(null);
    }
  }

  Future<bool> updateHabit(HabitsCompanion entry) =>
      update(habits).replace(entry);

  Future<bool> deleteHabit(HabitsCompanion entry) =>
      delete(habits).delete(entry).then((value) => value != 0);

  Future<int> deleteAllHabits() => delete(habits).go();

  List<Future<HabitsCompanion?>> insertAllHabits(
      List<HabitsCompanion> entries) {
    return entries.map((entry) => insertHabit(entry)).toList();
  }
}

@UseDao(tables: [Requests])
class RequestDao extends DatabaseAccessor<AppDatabase> with _$RequestDaoMixin {
  final AppDatabase database;

  RequestDao(this.database) : super(database);

  Future<List<RequestEntity>> get allRequests => select(requests).get();

  Stream<List<RequestEntity>> watchAllRequests() => select(requests).watch();

  Future<RequestEntity?> findRequestByHabitUid(String uid) {
    return (select(requests)..where((t) => t.habitUid.equals(uid)))
        .getSingleOrNull();
  }

  Future<int> _insertRequest(RequestsCompanion entry) {
    try {
      return into(requests).insert(entry, mode: InsertMode.insertOrFail);
    } catch (error) {
      return Future.value(null);
    }
  }

  Future<int> insertRequest(String habitUid, ActionType type,
      {DateTime? extra}) {
    var request = RequestsCompanion(
        habitUid: Value(habitUid), type: Value(type), extra: Value(extra));
    return _insertRequest(request);
  }

  Future<bool> deleteRequest(RequestsCompanion entry) =>
      delete(requests).delete(entry).then((value) => value != 0);
}

@UseMoor(tables: [Habits, Requests], daos: [HabitDao, RequestDao])
class AppDatabase extends _$AppDatabase {
  AppDatabase()
      : super(FlutterQueryExecutor.inDatabaseFolder(
            path: 'db.sqlite', logStatements: true));

  @override
  int get schemaVersion => 1;
}
