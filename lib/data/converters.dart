part of 'repository.dart';

extension EntityConverter on Habit {
  HabitEntity toEntity() => HabitEntity(
        uid: this.id,
        title: this.title,
        description: this.description,
        priority: this.priority.index,
        type: this.type.index,
        count: this.periodicity.repetitionsNumber,
        frequency: this.periodicity.daysNumber,
        color: this.color,
        date: this.date,
        doneDates: this.doneDates,
      );

  HabitsCompanion toCompanion() => this.toEntity().toCompanion(true);
}

extension ModelConverter on HabitEntity {
  Habit toModel() => Habit(
        id: this.uid,
        title: this.title,
        description: this.description,
        periodicity: Periodicity(
            repetitionsNumber: this.count, daysNumber: this.frequency),
        type: HabitType.values[this.type],
        priority: HabitPriority.values[this.priority],
        color: this.color,
        date: this.date,
        doneDates: this.doneDates,
      );
}
