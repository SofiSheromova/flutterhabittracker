import 'package:dio/dio.dart';
import 'package:flutterhabittracker/data/database/app_database.dart';
import 'package:flutterhabittracker/data/network/model/habit_done.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import 'model/habit_uid.dart';

// Это должно храниться где-нибудь не здесь
const String AUTHORISATION_TOKEN = '6395dfd0-b640-487a-99b7-1cfc567ca457';

class DioClient {
  static const String BASE_URL = 'https://droid-test-server.doubletapp.ru/api/';
  static const String GET_PATH = '/habit';
  static const String UPDATE_PATH = '/habit';
  static const String DELETE_PATH = '/habit';
  static const String MARK_DONE_PATH = '/habit_done';

  final Dio _dio = Dio(BaseOptions(
      baseUrl: BASE_URL, headers: {'Authorization': AUTHORISATION_TOKEN}));

  DioClient() {
    _dio.interceptors.add(PrettyDioLogger());
  }

  Future<List<HabitEntity>> getHabits() async {
    Response response = await _dio.get(GET_PATH);

    if (response.statusCode == 200) {
      var habitsData = response.data;

      return List<HabitEntity>.from(
          habitsData.map((model) => HabitServerJson.fromJson(model)));
    }

    throw DioError(requestOptions: response.requestOptions);
  }

  Future<HabitUid> updateHabit(HabitEntity entity) async {
    Response response =
        await _dio.put(UPDATE_PATH, data: entity.toJson()..remove('doneDates'));

    if (response.statusCode == 200) {
      return HabitUid.fromJson(response.data);
    }

    throw DioError(requestOptions: response.requestOptions);
  }

  Future<HabitUid> insertHabit(HabitEntity entity) {
    return updateHabit(entity.copyWith(uid: ""));
  }

  Future deleteHabit(HabitUid habitUid) async {
    Response response = await _dio.delete(DELETE_PATH, data: habitUid.toJson());

    if (response.statusCode == 200) {
      return;
    }

    throw DioError(requestOptions: response.requestOptions);
  }

  Future markDone(HabitDone habitDone) async {
    Response response =
        await _dio.post(MARK_DONE_PATH, data: habitDone.toJson());

    if (response.statusCode == 200) {
      return;
    }

    throw DioError(requestOptions: response.requestOptions);
  }
}

class HabitServerJson {
  static HabitEntity fromJson(json) {
    json['doneDates'] = (json['done_dates'] as List<dynamic>)
        .map((element) => element as int)
        .map((ms) => DateTime.fromMillisecondsSinceEpoch(ms))
        .toList();
    return HabitEntity.fromJson(json);
  }
}
