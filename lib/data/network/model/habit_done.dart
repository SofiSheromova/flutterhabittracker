class HabitDone {
  String uid;
  int date;

  HabitDone({required this.uid, required this.date});

  factory HabitDone.fromJson(Map<String, dynamic> json) {
    return HabitDone(uid: json['habit_uid'], date: json['date'] as int);
  }

  Map<String, dynamic> toJson() {
    return {'habit_uid': uid, 'date': date};
  }
}
