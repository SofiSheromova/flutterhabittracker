class HabitUid {
  final String uid;

  HabitUid(this.uid);

  factory HabitUid.fromJson(Map<String, dynamic> json) {
    return HabitUid(json['uid']);
  }

  Map<String, dynamic> toJson() {
    return {'uid': uid};
  }
}
