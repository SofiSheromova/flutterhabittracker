import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutterhabittracker/data/database/app_database.dart';
import 'package:flutterhabittracker/data/network/client.dart';
import 'package:flutterhabittracker/domain/model/habit.dart';
import 'package:flutterhabittracker/domain/model/habit_priority.dart';
import 'package:flutterhabittracker/domain/model/habit_type.dart';
import 'package:flutterhabittracker/domain/model/periodicity.dart';
import 'package:moor/moor.dart';

import 'network/model/habit_done.dart';
import 'network/model/habit_uid.dart';

part 'converters.dart';

class Repository {
  HabitDao habitDao;
  RequestDao requestDao;
  DioClient _client;

  Repository({required this.habitDao, required this.requestDao})
      : this._client = DioClient() {
    var habitsUpdated = false;

    requestDao.watchAllRequests().listen((entities) async {
      await _fulfillRequests(entities);

      if (entities.isEmpty && !habitsUpdated) {
        habitsUpdated = await _refreshHabits();
      }
    });
  }

  Stream<List<Habit>> watchAllHabits() {
    return habitDao.watchAllHabits().map((habitEntities) =>
        habitEntities.map((entity) => entity.toModel()).toList());
  }

  Future<HabitsCompanion?> insertHabit(Habit habit) {
    requestDao.insertRequest(habit.id, ActionType.insert);

    return habitDao.insertHabit(habit.toCompanion());
  }

  Future<HabitsCompanion?> updateHabit(String id, Habit newValue) {
    var habit = newValue.copyWith(id: id);

    requestDao.findRequestByHabitUid(habit.id).then((requestEntity) {
      if (requestEntity != null) return Future.value(requestEntity.id);

      return requestDao.insertRequest(habit.id, ActionType.update);
    });

    var companion = habit.toCompanion();
    return habitDao.updateHabit(companion).then((success) {
      if (success) return companion;
      return null;
    });
  }

  Future<bool> deleteHabit(Habit habit) {
    requestDao.findRequestByHabitUid(habit.id).then((requestEntity) {
      if (requestEntity != null && requestEntity.type == ActionType.insert)
        return requestDao.deleteRequest(requestEntity.toCompanion(true));

      return requestDao.insertRequest(habit.id, ActionType.delete);
    });

    return habitDao.deleteHabit(habit.toCompanion());
  }

  Future<Habit> markHabitDone(Habit habit, DateTime date) {
    var markedHabit = habit.copyWith()..markDone(date);

    requestDao.findRequestByHabitUid(habit.id).then((requestEntity) {
      if (requestEntity == null || requestEntity.type != ActionType.insert) {
        requestDao.insertRequest(habit.id, ActionType.markDone, extra: date);
      }
    });

    return habitDao
        .updateHabit(markedHabit.toCompanion())
        .then((success) => success ? markedHabit : habit);
  }

  Future<bool> _refreshHabits() {
    return _client
        .getHabits()
        .then(_updateLocalHabits)
        .then((_) => true)
        .catchError((error) => false);
  }

  Future<List<HabitsCompanion?>> _updateLocalHabits(
      List<HabitEntity> entities) {
    return habitDao
        .deleteAllHabits()
        .then((_) => habitDao.insertAllHabits(
            entities.map((entity) => entity.toCompanion(true)).toList()))
        .then((futures) => Future.wait(futures));
  }

  Future _fulfillRequests(List<RequestEntity> entities) async {
    if (entities.isEmpty) return;

    var entity = entities.first;
    try {
      await _fulfillRequest(entity);
    } on DioError catch (error) {
      var statusCode = error.response?.statusCode;
      if (statusCode == null || statusCode >= 500) return;
    }

    await requestDao.deleteRequest(entity.toCompanion(true));
  }

  Future _fulfillRequest(RequestEntity requestEntity) {
    switch (requestEntity.type) {
      case ActionType.insert:
        return _insertRemotely(requestEntity);

      case ActionType.update:
        return _updateRemotely(requestEntity);

      case ActionType.delete:
        return _deleteRemotely(requestEntity);

      case ActionType.markDone:
        return _markDoneRemotely(requestEntity);
    }
  }

  Future _insertRemotely(RequestEntity requestEntity) {
    return habitDao.getHabitByUid(requestEntity.habitUid).then((habitEntity) {
      if (habitEntity == null)
        throw InvalidDataException(
            'no habit with id ${requestEntity.habitUid}');
      return _insertHabitRemotely(habitEntity);
    }).then((habitEntity) {
      for (var date in habitEntity.doneDates) {
        requestDao.insertRequest(
          habitEntity.uid,
          ActionType.markDone,
          extra: date,
        );
      }
    });
  }

  Future<HabitEntity> _insertHabitRemotely(HabitEntity habitEntity) {
    return _client
        .insertHabit(habitEntity)
        .then((habitUid) => _updateLocalUid(habitEntity, habitUid));
  }

  Future<HabitEntity> _updateLocalUid(
      HabitEntity habitEntity, HabitUid habitUid) {
    var companion = habitEntity.toCompanion(true);

    return habitDao
        .deleteHabit(companion)
        .then((_) =>
            habitDao.insertHabit(companion.copyWith(uid: Value(habitUid.uid))))
        .then((_) => habitEntity.copyWith(uid: habitUid.uid));
  }

  Future _updateRemotely(RequestEntity requestEntity) {
    return habitDao.getHabitByUid(requestEntity.habitUid).then((habitEntity) {
      if (habitEntity == null) return Future.value(null);
      return _updateHabitRemotely(habitEntity);
    });
  }

  Future<HabitUid> _updateHabitRemotely(HabitEntity entity) {
    return _client.updateHabit(entity);
  }

  Future _deleteRemotely(RequestEntity requestEntity) {
    return _client.deleteHabit(HabitUid(requestEntity.habitUid));
  }

  Future _markDoneRemotely(RequestEntity requestEntity) {
    return _client.markDone(HabitDone(
        uid: requestEntity.habitUid,
        date: requestEntity.extra!.millisecondsSinceEpoch));
  }
}
